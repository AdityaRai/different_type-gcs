// Terraform plugin for creating random IDs
resource "random_id" "instance_id" {
  byte_length = 4
}
resource "google_storage_bucket" "bucket" {
#Number of bucket
 #count = 1
#Bucket name
 name = "adityabucket-${random_id.instance_id.hex}"
labels = {
    key = "env" 
    value = "dev"
  }
# Any location of your choice
 location = var.location
# Any storage_class of your choice
 #storage_class = "STANDARD"
 storage_class = var.storage_class
#storage_class = "ARCHIVE"
uniform_bucket_level_access = true
force_destroy = true

retention_policy {
  retention_period = 1000
  is_locked = true
}

lifecycle_rule {
    condition {
      age = "1"
    }
    action {
     type = "Delete"
    }
  }
}





  







 



